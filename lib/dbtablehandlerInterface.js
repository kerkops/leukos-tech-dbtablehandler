let {ancestors, custErrors, validators} = require('leukos-tech-jsutils');
// let {validateTableParams} = require('./validators');
let parameters_schema = require('./parameters_schema');

/** Genera il messaggio per segnalare la mancata implementazione del metodo `mName` */
function notImplemented(mName) {return `!! NON IMPLEMENTATO !! \`${mName}\` !! NON IMPLEMENTATO !!`}

/**
 * Definisce l'interfaccia degli oggetti di tipo Leukos-Tech-DbTableHandler
 */
class DbTableHandlerInterface extends ancestors.LoggedClass {
    // class DbTableHandlerInterface  {
    /**
     * Inizializza l'oggetto, rendendolo operativo
     * 
     * @param {Object} params Parametri per l'inizializzazione dell'oggetto
     * 
     * @async Promise(err, Boolean)
     * 
     * La promise si risolve in `true` se l'inizializzazione va a buon fine e l'oggetto diviene operativo
     * 
     * @throws `MissingArgError` se una delle proprietà necesarie non è inclusa
     * @throws `WrongArgTypeError` se una delle proprietà è di un  `type` errato.
     */
    async init(params, schema) { 
        
        // Validazione parametri 
        // let valid = validateTableParams(params);
        let valid = validators.validateObject(params, parameters_schema);
        
        if (!valid.success) {
            if (valid.problem == 'MISS') {
                throw new custErrors.args.MissingArgError(valid.propertyName)
            } else {
                throw new custErrors.args.WrongArgTypeError(valid.propertyName, valid.value, valid.expected)
            }
        }
        this._debug("VALIDAZIONE SUPERATA - Chiamata al metodo _init() interno...")
        return await this._init(params, schema); 
    };



    /**
     * Inserisce un nuovo record coi valori forniti
     * @param {Array} values Array di valori per il nuovo record 
     * 
     * @async Promise (err, QueryResult)
     */
    async insert(values) { return this._insert(values); };



    /**
     * 
     * @param {Array} keys Chiavi di cui si forniranno i valori per il matching
     * @param {Array} values Valori da associare alle chiavi, procedendo per indice dell'Array
     * 
     * @throws {RangeError} Se chiavi e valori non sono della stessa lunghezza
     * 
     * @async Promise(err, QueryResult)
     * 
     * Se nessun argomento viene fornito restituisce l'intero contenuto della table 
     */
    async find(keys=null, values=null) { return this._find(keys, values); };


    /**
     * Trova il record identificato dallo `id` fornito come argomento
     * @param {any} id Id del record
     * @param {String} idKey Stringa utilizzata come chiave per il campo con l'id dei record
     * 
     * @async Promise (err, QueryResult)
     */
    async findById(id, idKey="id") { return this._findById(id, idKey); };



    /**
     * Aggiorna i record identificati dagli *id* nell'array `ids`, inserendo alle chievi fornite in `keys` i nuovi valori forniti in `values`.
     * 
     * @param {Array} ids Array degli id dei record da aggiornare
     * @param {Array} keys Array delle chiavi di cui aggiornare i valori
     * @param {Array} values Array dei valori da aggiornare, aggiornati nei campi identificati da `keys` in accordo con gli indici
     * 
     * @async Promise (err, QueryResult)
     */
    async update(ids, keys, values, idkey="id") { return this._update(ids, keys, values, idkey); };



    /**
     * Elimina dalla Table i record con gli id specificati come argomento
     * 
     * @param {Array} ids Array degli id dei record da eliminare
     * @param {String} idKey Stringa utilizzata come chiave per il campo con l'id dei record
     * 
     * @async Promise (err, QueryResult)
     * 
     * Proprietà significative di `queryResult`
     * * `success` {Boolean} `true` se l'eliminazione è avvenuta con successo per almeno uno degli id forniti
     * * `message` {String} Valorizzato se `success: false` con la ragione dell'insuccesso
     * * `affected` {Number} Numero dei record eliminati
     */
    async delete(ids) { return this._delete(ids); };

    /**
     * Svuota la table da tutti i record
     * 
     * Da utilizzarsi durante lo sviluppo
     * 
     * @async Promise (err, Boolean) Risolta in `true` se l'operazione è andata a buon fine
     */
    async clear() {
        return this._clear();
    };

    /**
     * Elimina la table dal database, chiudendo la connessione
     */
    // async deleteTable() {
    //     return this._deleteTable()
    // }

    /** 
     * Arresta l'istanza dela pool comune a tutti i driver 
     * 
     * @param {Boolean} force Se true chiude la connessione al DB anche se l'istanza del Singleton è ancora attiva in altri oggetti.
     * 
     * 
     * */
    async stop(force=false) {
        
        if (this._copies == 0 || force) {
            return this._stop();
        } else {
            this._copies--;
            this._debug(`Decrementato il contatore dell'istanze del Singleton: ${this._copies} rimaste.`)
            return false
        } 
    };

    _init() {this._warn(notImplemented('_init(params)'))};
    _insert() {this._warn(notImplemented(`_insert(values)`))};
    _find() {this._warn(notImplemented(`_find(keys, values)`))};
    _findById() {this._warn(notImplemented(`_findById(id)`))};
    _update() {this._warn(notImplemented(`_update(ids, keys, values)`))};
    _delete() {this._warn(notImplemented(`_delete(ids)`))};
    _stop() {this._warn(notImplemented(`_stop()`))};
    _clear() {this._warn(notImplemented(`_clear()`))};
    _deleteTable() {this._warn(notImplemented(`_deleteTable()`))};
};

module.exports = DbTableHandlerInterface; 