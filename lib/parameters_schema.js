/**
 * Definische lo schema per la validazione dei parametri di `validators.validateTableParams()`
 */
module.exports = {
    
    "database": "string",
    "user": "string",
    "password": "string",
    "table": "string"

};