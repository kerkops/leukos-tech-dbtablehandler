
// Packages Leukos-Tech
const jsutils = require('leukos-tech-jsutils');
const dbdriver = require('leukos-tech-dbdriver');

// Errori custom
const argErrs = jsutils.custErrors.args

// Interfaccia da realizzare
const TableHandlerInterface = require('./dbtablehandlerInterface');




class MySqlTableHandler extends TableHandlerInterface {

    /**
     * 
     * @param {Boolean} debug Se `true` abilita il debug secondo il `logger` fornito (default `false`).
     * @param {Object} logger Oggetto da utilizzare per il logging (default `console`).
     * @param {Number} logLevel Livello del logging (se attivo): 0 debug, 1 info, 2 warn, 3 *error* (default 1) 
     */
    constructor (debug=false, logger=console, logLevel=1) {

        // Inizializza gli ancestors (TablehandlerInterface -> LoggedClass)
        super(debug, logger, logLevel);
        
        /** @property Tipo di engine */
        this._engine = "mysql";
        
        /** @property Nome della table gestita da quest'oggetto */
        this._tableName = null;
        /** @property Contiene l'oggetto `Leukos-Tech-DbDriver` utilizzato per interagire col Db */
        this._driver = null;
        /** @property Contiene l'array delle stringhe che intitolano i vari campi dei record */
        this._tableKeys = null;


        // this._debug(`DEBUG ATTIVO: ${this._debugEnabled} - LIVELLO: ${this._logLevel}`);
        // this._debug("Fine COSTRUTTORE");

    };


    /**
     * Inizializza l'handler secondo i parametri forniti come argomento
     * 
     * **L'argomento `tableParams` è sempre valido, in quanto già verificato dal metodo pubblico `.init(tableParams)` prima di venir passato a questo.**
     * 
     * @param {Object} tableParams Parametri per l'inizializzazione dell'handle
     */
    async _init(tableParams, schema) {

        tableParams = jsutils.io.copyObj(tableParams);
        this._tableName = tableParams.table;
        delete tableParams.table;

        this._debug(`Valorizzato attributo \'_tableName\' in "${this._tableName}" `);
        let driver = new dbdriver.driverFactory(this._engine, this._debugEnabled, this._logger, this._logLevel);
        this._debug("Generato il driver")
        this._debug(`Parametri in uso: ${JSON.stringify(tableParams)}`)
        try {
            let initDone = await driver.init(tableParams);

            if (initDone) {
                let tableReady = await driver.ensureTable(this._tableName, schema);
                if (tableReady.success) {

                    // Valorizza la proprietà col driver
                    this._driver = driver;
                    
                    // Memorizza le stringhe coi nomi dei record secondo l'ordine
                    // Versione che esclude i campi autoincrement
                    this._insertKeys = [];
                    // Versione con tutte le chiavi
                    this._tableKeys = []
                    for (let item of schema) {
                        this._tableKeys.push(item.keyName) 
                        if (!item.autoIncrement) {
                            this._insertKeys.push(item.keyName) 
                        };
                    };
                    this._debug(`Keys per la table "${this._tableName}": ${this._tableKeys}`);
                    this._debug(`Keys per l'insert' "${this._tableName}": ${this._insertKeys}`);
                    return true

                } else {
                    this._warn(`Impossibile accedere alla table "${this._tableName}": ${tableReady.message}`)
                }

                
            } else {
                this._warn(`Fallita inizializzazione dell'handler per la table "${this._tableName}"`);
            }

            return false
        } catch (err) {
            this._error(err, `Impossibile inizializzare il TableHandler`);
            return false
            
        }



    };

    /**
     * Azzera il contenuto della table
     */
    async _clear() {
        try {
            if (this._driver && this._driver.poolEnabled() && this._tableName != null) {
                let sql = "TRUNCATE TABLE "+this._driver._dbName+"."+this._tableName+";";
                let res = await this._driver._pool.query(sql);
                if (res.constructor.name == 'OkPacket') {
                    this._debug(`Table "${this._tableName}" svuotata con successo`);
                    return true;
                } else {
                    this._warn(`Impossibile svuotare la table "${this._tableName}": Pacchetto ricevuto: ${JSON.stringify(res)}`)
                }
            } else {
                this._warn(`Impossibile azzerare la table. Condizioni non sufficienti`)
            }
            return false;
        } catch(err) {
            this._error(err, 'Impossibile Azzerare la Table');
            return false;
        }
    };

    /** Arresta il dbDriver alla base dell'oggetto */
    async _stop() {
        return await this._driver.stop()
    };

    /**
     * Inserisce un nuovo record coi valori forniti
     * @param {Array} values Array di valori per il nuovo record 
     * 
     * @async Promise (err, QueryResult)
     */
    async _insert(values) {
        try {

            // Values fornito con il numero corretto di elementi
            if (values.length == this._insertKeys.length) {
                // this._debug("Valori della lunghezza corretta")
                return await this._driver.insert(this._tableName, this._insertKeys, values);
            
            } else if (values.length == this._tableKeys.length) {
                this._warn("Forniti valori ANCHE per i campi AUTO-INCREMENT")                
                return await this._driver.insert(this._tableName, this._tableKeys, values)
            
            } else return dbdriver.queryFail(`\n\tMysqlTableHandler.insert(values)\n\tL'argomento 'values' ha un numero errato di elementi\n\tCampi richiesti per 1 record (${this._insertKeys.length}): [${this._insertKeys}]. \n\tElementi nell'array fornito  (${values.length}): [${values}]\n`);
                
        } catch (error) {
            return dbdriver.queryFail("Errore durente il metodo Insert", null, null, values);
            
        }
        
    };

    
    
    /**
     * Restituisce il/i record/s corrispondenti ai parametri forniti
     * 
     * @param {Array} keys Chiavi di cui si forniranno i valori per il matching
     * @param {Array} values Valori da associare alle chiavi, procedendo per indice dell'Array
     * 
     * @throws {RangeError} Se chiavi e valori non sono della stessa lunghezza
     * 
     * @async Promise(err, QueryResult)
     * 
     * Se nessun argomento viene fornito restituisce l'intero contenuto della table 
     */
    async _find(keys=null, values=null) {
        try {
            let res = await this._driver.find(this._tableName, keys, values);

            if (res.data.length == 0) res.success = false;
            return res
        
        
        } catch (err) {
            return dbdriver.queryFail(`Errore nel metodo MySqlTableHandler._find(keys, values) - ${err.name}: ${err.message}.`);
        }
    };



    /**
     * Trova il record identificato dallo `id` fornito come argomento
     * @param {any} id Id del record
     * @param {String} idKey Stringa utilizzata come chiave per il campo con l'id dei record
     * 
     * @async Promise (err, QueryResult)
     */
    async _findById(id=null, idKey="id") {
        try {
            if (!id) { return await this._driver.find(this._tableName) }

            let res = await this._driver.find(this._tableName, ["id"], [id]);
            
            if (res.data.length == 0) res.success = false;
            
            return res
            
        } catch (err) {
            this._error(err, "_findById() - ERRORE IMPREVISTO")
            return dbdriver.queryFail(`Errore nel metodo MySqlTableHandler._findById(${id}) - ${err.name}: ${err.message}.`);
            
        }
    };



    /**
     * Aggiorna i record identificati dagli *id* nell'array `ids`, inserendo alle chievi fornite in `keys` i nuovi valori forniti in `values`.
     * 
     * @param {Array} ids Array degli id dei record da aggiornare
     * @param {Array} keys Array delle chiavi di cui aggiornare i valori
     * @param {Array} values Array dei nuovi valori per i campi `keys`
     * @param {String} idKey Stringa identificativa per il campo *id* della *table* (default `"id"`)
     * 
     * @async Promise (err, QueryResult)
     */
    async _update(ids, keys, values, idKey="id") {
        try {
            return this._driver.update(this._tableName, ids, keys, values, idKey)
            
        } catch (error) {
            this._error(err, "_update() - ERRORE IMPREVISTO")
            return dbdriver.queryFail(`Errore nel metodo MySqlTableHandler._update() - ${err.name}: ${err.message}.`);
            
        }
    };

    /**
     * Elimina dalla Table i record con gli id specificati come argomento
     * 
     * @param {Array} ids Array degli id dei record da eliminare
     * @param {String} idKey Stringa utilizzata come chiave per il campo con l'id dei record
     * 
     * @async Promise (err, QueryResult)
     */
    async _delete(ids, idKey="id") { 
        try {
            return await this._driver._delete(this._tableName, ids, idKey)
        } catch (err) {
            this._error(err, "_delete() - ERRORE IMPREVISTO")
            return dbdriver.queryFail(`Errore nel metodo MySqlTableHandler._delete() - ${err.name}: ${err.message}.`);
            
        }
    };

    


};


module.exports = MySqlTableHandler;