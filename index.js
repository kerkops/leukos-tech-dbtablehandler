const MysqlTableHandler = require('./lib/mysql_tablehandler');
const jsutils = require('leukos-tech-jsutils');

let INSTANCES = new Map();

/**
 * Oggetto per i parametri di Inizializzazione
 * @typedef {Object} TableParams
 * @property {String} engine Specifica dell'`engine` da utilizzare
 * @property {String} database Nome del database su cui operare [Mandatory]
 * @property {String} table Nome della table su cui operare [Mandatory]
 * @property {String} user Nome utente per l'autenticazione presso il database engine [Mandatory]
 * @property {String} password Password da utilizzare per l'autenticazione presso il database engine [Mandatory]
 * @property {String} host Indirizzo dell'host che ospita l'engine del database (se non fornito 'localhost' verrà utilizzato)
 * @property {Number} port Porta di ascolto del database engine (se non fornito quella di *default* verrà utilizzata)
 * @property {Number} connectionLimit Numero massimo di connessioni contemporanee consentite all'host del database
 *  
 */


/**
 * Genera l'oggetto
 * @param {TableParams} params Oggetto contenente i parametri di inizializzazione dell'handler
 * @param {Array} schema Array di oggetti `TableSchema` per definire la struttura della `table` e crearla in caso non esista
 * @param {Boolean} debugEnabled Se `true` il debug è attivo (default `false`)
 * @param {Number} logLevel Livello di logging nel caso il debug sia attivo (default 1 - INFO)
 * @param {Object} logger Oggetto da utilizzare per il logging (default `console`)
 * 
 * @async Promise (err, tableHandler||undefined)
 * 
 * Se l'inizializzazione avviene con successo restituisce l'handler, altrimenti `undefined`
 * 
 * 
 */
async function factory (params, schema, debugEnabled=false, logLevel=1, logger=console) {

    let instance = INSTANCES.get(params);

    // Se ha trovato l'istanza restituisce il Singleton
    if (instance != undefined) {
        if (debugEnabled) logger.debug(`Restituita copia del Singleton: ${instance.handlerName}`);
        instance._copies++;
        return instance;
    }

    if (!params.engine) throw new jsutils.custErrors.args.MissingArgError('engine');

    // Crea una copia di lavoro sicura dei parametri
    let safeParams = jsutils.io.copyObj(params);

    // estrae la proprietà 'engine' e la elimina dall'oggetto dei parametri
    let engine = String(safeParams.engine).toLowerCase();
    delete safeParams.engine;

    switch (engine) {

        case 'mysql':
            

            instance = new MysqlTableHandler(debugEnabled, logger, logLevel);

            try {
                let initDone = await instance.init(safeParams, schema);

                
                if (initDone) {
                    // Stringa identificativa per verifiche sul singleton
                    instance.handlerName = Math.random().toString(36);
                    instance._copies = 0;
                    // Memorizza l'oggetto tra le istanze
                    INSTANCES.set(params, instance);
                    if (debugEnabled) logger.debug(`Generata nuova istanza del Singleton: ${instance.handlerName}`);
                    // restituisce l'istanza
                    return instance;
                };

                logger.error(`Errore nell'inizializzazione dell'istanza del TableHandler: \`MysqlTableHandler.init()\' ha restituito \'false\'.`)
                return undefined
            } catch (err) {
                logger.error(`Errore nell'inizializzazione dell'istanza del TableHandler: ${err.name} - ${err.message}`);
                return undefined;
            }
            

        default:
            logger.warn(`Factory di Leukos-Tech-TableHandler: engine '${engine}' non supportato. IMPOSSIBILE GENERARE L'OGGETTO. Verifica la proprietà 'engine' dei parametri forniti`);
            return undefined;
    
    }


};


module.exports = factory;