# Leukos-Tech-DbTableHandler

Handler per database tables.

Wrapper per Drivers di tipo `Leukos-Tech-DbDriver`([vai alla documentazione](https://www.npmjs.com/package/leukos-tech-dbdriver)), consente la gestione di una specifica table.

Package per `npm`.


<a name='summary'></a>

## Sommario

* [ Installazione ](#install)
* [ Import della *factory* e suo utilizzo ](#factory)
    * [ Oggetto con i parametri di inizializzazione ](#tableparams)
    * [ Oggetti `TableField` (elementi dell'array `schema`) ](#tablefield)
    * [ Factory e *Singleton pattern* ](#singleton)
* [ Metodi della classe `Leukos-Tech-DbTableHandler ](#methods)
    * [ DbTableHandler.insert(values) ](#insert)
    * [ DbTableHandler.find(keys, values) ](#find)
    * [ DbTableHandler.findById(id, idkeys="id") ](#findbyid)
    * [ DbTableHandler.update(ids, keys, values, idKey="id")](#update)
    * [ DbTableHandler.delete(ids, idKey="id") ](#delete)
    * [ DbTableHandler.stop(force=false) ](#stop)


<a name='install'></a>

## Installazione

```bash
$ npm install leukos-tech-dbtablehandler --save
```

<a name='factory'></a>

## Import della *factory* e suo utilizzo

L'*import* del *package* nel *nameSpace* restituisce un oggetto asincrono (*factory*) da utilizzare per generare l'istanza del *tableHandler*.

La factory richiede i seguenti argomenti:

* `params` **{Object}** Oggetto con i parametri per l'inizializzazione [Mandatory]
* `schema` **{Array}** Array di oggetti `TableField` per definire i campi della table e le sue proprietà [Mandatory]
* `debugEnabled` **{Boolean}** Se `true` attiva il logging dell'oggetto (default `false`)
* `logLevel` **{Number}** Livello di loogging dell'oggetto se `debugEnabled` è `true` (default 1 - INFO)
* `logger` **{Object}** Oggetto da utilizzare per il logging (default `console`). **NB. Tale oggetto viene utilizzato a prescindere da `debugEnabled` e `logLevel` per il logging di *warnings* ed errori.**

La *factory* opera in modo *asincrono*, restituendo una *Promise* che può essere risolta nell'oggetto `tableHandler` o `undefined` a seconda del successo o fallimento dell'operazione di inizializzazione.

Solleva `MissingArgError` se manca la proprietà `engine` nell'oggetto `params`.

Esempio:

```javascript
const handlerFactory = require('leukos-tech-dbtablehandler');

// 1. Utilizzo con la notazione standard delle Promises
handlerFactory(params, schema, true, 0, console)
.than( (tableHandler) => {
    if (tableHandler) {
        // L'inizializzazione ha avuto successo
        ...
    } else {
        // L'inizializzazione è fallita
    };
})
.catch ( (err) => {
    // Non fornita la proprietà 'engine'
    if (err.name === 'MissingArgError') {
        ...
    // Errore inatteso
    } else {
        ...
    }
});

// 2. Utilizza con la notazione async/await
try {
    let tableHandler = await handlerFactory(params, schema, true, 0, console);

    if (tableHandler) {
        // L'inizializzazione ha avuto successo
        ...
    } else {
        // L'inizializzazione è fallita
        ...
    };
} catch (err) {
    if (err.name === 'MissingArgError') {
        // Non fornito il parametro 'engine'
        ....
    } else {
        // Errore imprevisto
    }
};
```

[ Torna al Sommario ](#summary)

<a name='tableparams'></a>

### Oggetto con i parametri di inizializzazione

L'oggetto `params` da fornire come 1° argomento alla *factory* **deve possedere** le seguenti proprietà:

* `engine` **{String}** Engine del database da utilizzare (correntemente supportati: "mysql")
* `database` **{String}** Nome del database da utilizzare
* `table` **{String}** Nome della table gestita dall'*handler*
* `user` **{String}** Nome utente per l'autenticazione presso il *database engine*
* `password` **{String}** Password da utilizzare per l'autenticazione presso il *database engine*

Le seguenti proprietà sono invece opzionali:

* `connectionLimit` **{Number}** Numero massimo di connessioni contemporanee al *database engine* consentite.
* `host` **{String}** Indirizzo di residenza del *database engine* (se non fornito "localhost" verrà utilizzato)
* `port` **{Number}** Porta di ascolto del *database engine* (se non fornito quella di *default* verrà utilizzata).

<a name='tablefield'></a>

### Oggetto `TableField` (elementi dell'array `schema`)

L'array `schema`, da fornire come 2° argomento alla *factory* deve essere composto da oggetti di tipo `TableField`, ciascuno dei quali definisce le proprietà di un signolo campo dei record nella table che l'handler andrà a gestire. L'ordine degli elementi determina (per engine compatibili) l'ordine dei campi nei record. Gli oggetti che compongono l'array debbono essere strutturati secondo le seguenti proprietà:

* `keyName` **{String}** Nome del campo corrente

<br>

* `type` **{String}** Tipologia di dato che conterrà. Valori accettati per questa proprietà sono:
    * **'string'**, **'char'** per datatype letterali
    * **'number'**, **'int'**, **'integer'**, **'intero'** per datatype numerici di tipo **integer**
    * **'float'**, **'double'** per datatype numerici di tipo **float**
    * **'bool'**, **'boolean'** per datatype **booleani**
    * **'date'**, **'datetime'**, **'time'**, **'timestamp'** per datatype **temporali**
    * **'blob'**, **'image'**, **'audio'**, **'mediumblob'**, **'longblob'** per datatype **bit-based**

<br>

* `size` **{Number}** Dimensioni del dato, secondo le seguenti regole: 
    * Viene **ignorato** per i *type* **"blob"**, **"image"**, **"audio"**, **"mediumblob"**, **"longblob"** (sempre restituito un **"LONGBLOB"**).
    * Per il *type* **"string"** se il valore fornito supera il valore fissato in *MAX_VARCHAR_SIZE* viene restituito **"TEXT"**. Altrimenti **"VARCHAR(s)"** con *s=size*. Se *size* non viene fornito restituisce **"VARCHAR(255)"**
    * Per il *type* **"char"** il **valore massimo possibile è 255**. Con valori superiori, uguali a 0, o el caso non sia fornito affatto, la funzione solleva *RangeError*.
    * Per *type* **"number"**, **"int"**, **"integer"**, **"intero"**
        - con `size==undefined`* o `size==2` restituisce **"SMALLINT"**;
        - con `size==1` restituisce **"TINYINT"**;
        - con `size==3` restituisce **"MEDIUMINT"**;
        - con `size==4` restituisce **"INT"**;
        - con `size>=5`, `size<=8` restituisce **"BIGINT"**
    * Viene **ignorato** per gli altri *type*s.

<br>

* `isUnsigned` **{Boolean}** Se `true` il campo è un valore numerico *unsigned*

<br>

* `autoIncrement` **{Boolean}** Se `true` il campo provvede automaticamente al proprio incremento

<br>

* `notNull` **{Boolean}** Se `true` è necessario fornire un valore in caso di inserimento di nuovo record

<br>

* `isPrimary` **{Boolean}** Se `true` il campo costituisce una chiave primaria (id del record)

<br>

* `isIndex` **{Boolean}** Se `true` il campo viene indicizzato

<br>

* `isUnique` **{Boolean}** Se `true` non possono esserci valori ripetuti per questo campo nei vari record

<br>

* `isReference` **{Boolean}** Se `true` il campo è un riferimento ad un altra table. In questo caso è necessario fornire i valori `tableName` e `tableField` nella proprietà `reference`. Se false i valori in `reference` verranno ignorati.

<br>

* `reference` **{Object}** Oggetto utilizzato in caso `isReference` sia `true`; in tal caso deve possedere le proprietà:

    * `tableName` **{String}** Nome della *table* a cui questo campo fa riferimento

    * `tableField` **{String}** Nome del campo, nella table *tableName*, a cui questo fa riferimento

<a name='singleton'></a>

[ Torna al Sommario ](#summary)

### Factory e *Singleton Pattern*

La *factory* utilizza il *Signeton pattern*, in funzione dei parametri forniti con l'argomento `params`. 

Di conseguenza, al fornire degli stessi parametri, viene restituito sempre lo stesso oggetto. 

Lo stesso vale anche a livello del driver utilizzato: *tables* diverse non condividono lo stesso *Singleton* del *tableHandler* ma, se i parametri di connessione al database sono i medesimi, condivideranno il Singleton per la stessa istanza del driver.

Esempio:

```javascript
let params1 = {
    engine: "mysql",
    table: "table1", // Proprietà relativa soltanto all'handler
    database: "myDatabase", // Proprietà relativa al driver
    user: "leukos-tech", // Proprietà relativa al driver
    password: "averycomplicatedpassword" // Proprietà relativa al driver
};

let params2 = {
    engine: "mysql",
    table: "table2", // ---> Proprietà differente <---
    database: "myDatabase", // Proprietà relativa al driver (uguale a `params1`)
    user: "leukos-tech", // Proprietà relativa al driver (uguale a `params1`)
    password: "averycomplicatedpassword" // Proprietà relativa al driver (uguale a `params1`)
};

try { tableHandler1 = handlerFactory(params1, schema) } catch (err) {...}
try { tableHandler2 = handlerFactory(params1, schema) } catch (err) {...}
// tableHandler1 e tableHandler2 sono copie della stessa istanza
try { tableHandler3 = handlerFactory(params2, schema) } catch (err) {...}
// tableHandler1/2 e tableHandler3 sono istanze differenti dell'handler

tableHandler.newProperty = 1;

console.log(tableHandler2.newProperty)
// output: 1

console.log(tableHandler3.newProperty)
// output: undefined

// Tuttavia tableHandler1, tableHandler2, e tableHandler3 condividono la stessa istanza del driver
```

**TIP**: Nel caso fosse necessario ottenere differenti istanze (indipendenti) anche del driver, è possibile farlo assegnando un valore differente alla proprietà `connectionLimit`.
```javascript
let params3 = {
    engine: "mysql",
    table: "table1", // Proprietà relativa soltanto all'handler
    database: "myDatabase", // Proprietà relativa al driver
    user: "leukos-tech", // Proprietà relativa al driver
    password: "averycomplicatedpassword" // Proprietà relativa al driver
    connectionLimit: 100 // Proprietà relativa al driver
};

let params4 = {
    engine: "mysql",
    table: "table2", // Proprietà differente
    database: "myDatabase", // Proprietà relativa al driver (uguale a `params3`)
    user: "leukos-tech", // Proprietà relativa al driver (uguale a `params3`)
    password: "averycomplicatedpassword" // Proprietà relativa al driver (uguale a `params3`)
    connectionLimit: 101 // ---> Proprietà relativa al driver (differente da `params3`) <---
};

try { tableHandler4 = handlerFactory(params3, schema) } catch (err) {...}
try { tableHandler5 = handlerFactory(params4, schema) } catch (err) {...}
// Sono oggetti del tutto scollegati, anche nell'underlying driver

```

Ricapitolando:

1. Con gli stessi parametri per la *pool* e stesso nome della table restituisce sempre lo stesso oggetto (Singleton puro).

2. Con gli stessi parametri per la *pool* e nomi differenti per le tables, restituisce oggetti differenti (non correlati) che condividono  tuttavia lo stesso Singleton della *pool* (correlati).

3. Con parametri totalmente differenti restituisce oggetti differenti e non correlati in alcun modo tra loro.

[ Torna al Sommario ](#summary)

<br>

<a name='methods'></a>

# Metodi della classe `Leukos-Tech-DbTableHandler`

<a name='insert'></a>

## `DbTableHandler.insert(values {Array})`

Inserisce un nuovo record coi valori forniti

### Argomenti

* `values` **{Array}** Array dei valori per il nuovo record

### Tipi restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`      **{Boolean}**   `true` se l'inserimento si è svolta senza sollevare eccezioni.
* `message`      **{String}**    Vuoto se `success==true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
* `data`         **{Object}**    Oggetto *raw* restituito dalla libreria utilizzata come interfaccia al DB.
* `query` 	    **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
* `params`       **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
* `insertedIds`  **{Array}**     Array degli id dei nuovi record inseriti.
* `affected`     **{Number}**    Numero di record inseriti (`insertedIds.length`).

### Test Cases

* **INS01** - Risolve in `queryResult.success:true` ed inserisce i valori con campi conformi.
* **INS02** - Risolve in `queryResult.success:false` e non inserisce i valori con array di valori di lunghezza errata 
* **INS03** - Risolve in `queryResult.success:true` ed inserisce i valori con array che comprendono anche valori 'AUTO_INCREMENT' (campi id)

[ Torna al Sommario](#summary)

<br>

<a name='find'></a>

## `DbTableHandler.find(keys{Array}, values{Array})`

Restituisce il record (o i records) corrispondenti ai valori forniti nell'Array `values` per le chiavi `keys`

### Argomenti

* `keys` **{Array}** Array delle chiavi per cui verrà fornito il valore da individuare
* `values` **{Array}** Array dei valori, per chiavi `keys`, con cui identificare i *records*

**NB: se nessun argomento viene fornito, l'intero contenuto della table viene restituito in `queryResult.data`**

### Tipi Restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`  **{Boolean}**   `true` se la query si è svolta senza sollevare eccezioni (non comporta la restituzione di risultati).
* `message`  **{String}**    Vuoto se `success==true`. Altrimenti contiene una stringa indicante la ragione dell'insuccesso.
* `data`     **{Array}**     Array dei record corrispondenti ai parametri forniti. **Vuoto se nessun record corrisponde ai parametri.**
* `query`    **{String}**    Stringa di query (SE NECESSARIA) utilizzata  per l'interrogazione al database.
* `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di query per l'interrogazione al database.
* `affected` **{Number}**    Numero di risultati prodotti dalla query (`data.length`)

### Test Cases

* **FIN01** - Risolve in `queryResult.success: true` con parametri relativi ad un record esistente
* **FIN02** - Risolve in `queryResult.success: true` e restituisce tutti i record se non fornito nessun argomento
* **FIN03** - Risolve in `queryResult.success: false` e non restituisce alcun record se forniti argomenti non validi


[ Torna al Sommario ](#summary)

<br>

<a name='findbyid'></a>

## `DbTableHandler.findById(id{Any}=null, idKey{String}="id")`

Restituisce il record identificato dall'*id* fornito come argomento.

### Argomenti

* `id` **{Any}** Identificativo del record (se non fornito restituisce tutti i record nella table)
* `idKey` **{String}** Stringa identificativa del campo *id* della table (*default* `"id"`)

### Tipi Restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`  **{Boolean}**   `true` se la query si è svolta senza sollevare eccezioni (non comporta la restituzione di risultati).
* `message`  **{String}**    Vuoto se `success==true`. Altrimenti contiene una stringa indicante la ragione dell'insuccesso.
* `data`     **{Array}**     Array dei record corrispondenti ai parametri forniti. **Vuoto se nessun record corrisponde ai parametri.**
* `query`    **{String}**    Stringa di query (SE NECESSARIA) utilizzata  per l'interrogazione al database.
* `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di query per l'interrogazione al database.
* `affected` **{Number}**    Numero di risultati prodotti dalla query (`data.length`)

### Test Cases

* **FID01** - Risolve in `queryResult.success: true` e restituisce il record se fornito un id valido
* **FID02** - Risolve in `queryResult.success: true` e restituisce tutti i record se non fornito nessun id
* **FID03** - Risolve in `queryResult.success: false` e non restituisce alcun record se fornito un id non valido

[ Torna al Sommario ](#summary)

<br>

<a name='update'></a>

## `DbTableHandler.update(ids{Array}, keys{Array}, values{Array}, idkey{String}="id")`

Aggiorna i record identificati dagli *id* nell'array `ids`, inserendo alle chievi fornite in `keys` i nuovi valori forniti in `values`.

### Argomenti

* `ids` **{Array}** Array degli id dei record da aggiornare
* `keys` **{Array}** Array delle chiavi dei campi di cui aggiornare i valori
* `values` **{Array}** Array dei nuovi valori per i campi `keys`
* `idKey` **{String}** Stringa identificativa per il campo *id* della *table* (default `"id"`)

### Tipi Restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`  **{Boolean}**   `true` se l'aggiornamento valori si è svolto senza sollevare eccezioni.
* `message`  **{String}**    Vuoto se `success=true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
* `data`     **{Object}**    Oggetto *raw* restituito dalla libreria per la comunicazione col DB (per `mysql` è `OkPacket`). 
* `query` 	 **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
* `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
* `changed`  **{Number}**    Numero dei record modificaty dalla query.


### Test Cases

* **UPD01** - Risolve in `queryResult.success: true` e aggiorna i campi dei record con id valido e parametri corretti.
* **UPD02** - Risolve in `queryResult.success: false` e non aggiorna i campi dei record con id non valido.
* **UPD03** - Risolve in `queryResult.success: false` e non aggiorna i campi dei record con id valido e parametri non corretti

[ Torna al Sommario ](#summary)

<br>

<a name='delete'></a>

## `DbTableHandler.delete(ids{Array}, idKey{String}="id")`

Elimina dalla Table i record con gli id specificati come argomento.

### Argomenti

* `ids` **{Array}** Array degli id dei record da eliminare
* `idKey` **{String}** Stringa identificativa per il campo *id* della *table* (default `"id"`)

### Tipi restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success` **{Boolean}** `true` Se almeno un elemento è stato eliminato.
* `message` **{String}** Se `success=false` può contenenere il motivo dell'insuccesso.
* `query` **{String}** Stringa utilizzata per la query. 
* `parameters` **{Array}** Array di parametri utilizzati per la query.
* `affected` **{Number}** Numero di elementi eliminati.

### Test Cases

* **DEL01** - Risolve in  `queryResult.success: true` ed elimina il record se fornito un id valido

[ Torna al Sommario ](#summary)

<br>

<a name='stop'></a>

## `DbTableHandler.stop(force=false)`

Arresta la connessione al database, se nessun altra copia dell'istanza la sta utilizzando ancora (ossia la arresta effettivamente solo se tutte le altre copie hanno già chiamato questo metodo). E' un semplice *wrapper* per il metodo `.stop(force)` del `driver` ([ v. la documentazione di Leukos-Tech-DbDriver](https://www.npmjs.com/package/leukos-tech-dbdriver#arresto-del-driver) ).

### Argomenti

* `force` **{Boolean}** Se `true` arresta immediatamente la connessione al database (anche per istanze scollegate del `tableHandler` che però condividono la stessa istanza del driver - v. [Factory e *Singleton pattern*](#factory)). **L'UTILIZZO DI QUESTO ARGOMENTO E' DUNQUE FORTEMENTE SCONSIGLIATA** tranne nel caso di *crash-only apps* che necessitano un riavvio completo senza lasciarsi dietro *threads* aperti.

### Tipi Restituiti

`Promise (err, Boolean)`

Risolve in `true` solo se la connessione al database è stata **effetivamente** chiusa (rendendola inutilizzabile)

### Test Cases

* **STP01** - Risolve in `false` e mantiene aperta la pool se il metodo stop viene invocato con altre istanze in uso

[ Torna al Sommario ](#summary)
