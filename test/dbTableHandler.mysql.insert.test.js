
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require('../')
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED = (['1', 'true'].includes(env_DEBUG_PROP))
// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const expect = require('chai').expect;

const jsutils = require('leukos-tech-jsutils');
const getHeader = jsutils.tests.getHeader;

/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
/** Handler risultati timing */
const TimingResults = jsutils.tests.timeIt;

const hrtime = process.hrtime;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const schemas = require('./resources/tableSchemas');
const entries = require('./resources/dbEntries');
const params = require('./resources/tableParams');

// const aVar = require('');
// const aVar = require('');
// const aVar = require('');


describe (`${getHeader('MysqlTableHandler.insert(values {Array}): Promise(err, queryResult)', 0)}`, async function() {
    this.timeout(0)
    
    // Variabili con Scope locale a questa unità vanno qui
    let mainHeader = `MysqlTableHandler.insert(values {Array}): Promise(err, queryResult)`;
    let timeRes;
    let tableHandler;
    let cleared;
    let start;
    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`INS01 - Risolve in 'queryResult.success:true' ed inserisce i valori con campi conformi `, async function(){
        let thisHeader = 'INS01';
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!tableHandler) {
            console.warn(`\t# TEST SKIPPED: Non ottenuta istanza di tableHandler #`);
            this.skip();
        } 
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.insert(entries.users1.records[0]);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), [entries.users1.records[0]])
        if (DEBUG_ENABLED) console.debug(`${header} - queryResult.success: ${res.success}}`);
        if (DEBUG_ENABLED && !res.success) console.debug(`${header} - queryResult.message: ${res.message}}`);
        
        
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    it(`INS02 - Risolve in 'queryResult.success:false' e non inserisce i valori con array di valori di lunghezza errata`, async function(){
        let thisHeader = 'INS02';
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!tableHandler) {
            console.warn(`\t# TEST SKIPPED: Non ottenuta istanza di tableHandler #`);
            this.skip();
        } 
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.insert(['just', 3, 'values']);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), [['just', 3, 'values']])
        if (DEBUG_ENABLED) console.debug(`${header} - queryResult.success: ${res.success}`);
        if (DEBUG_ENABLED && !res.success) console.debug(`${header} - queryResult.message: ${res.message}`);
        
        
        
        expect(res.success).equal(false);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    it(`INS03 - Risolve in 'queryResult.success:true' ed inserisce i valori con array che comprendono anche valori 'AUTO_INCREMENT'`, async function(){
        let thisHeader = 'INS03';
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!tableHandler) {
            console.warn(`\t# TEST SKIPPED: Non ottenuta istanza di tableHandler #`);
            this.skip();
        } 
        let records = jsutils.io.copyObj(entries.users1.records[0]);
        // AGGIUNGE IL VALORE PER L'ID
        records.unshift(100);

        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.insert(records);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), [records])
        if (DEBUG_ENABLED) console.debug(`${header} - queryResult.success: ${res.success}}`);
        if (DEBUG_ENABLED && !res.success) console.debug(`${header} - queryResult.message: ${res.message}}`);
        
        
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

 /**
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  *                     IMPOSTAZIONI DEI TEST
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  */
    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    // before( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        let stopped = await tableHandler.stop();
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
        
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - after() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 200;
        tableHandler = await tested(par, schemas.userSch, DEBUG_ENABLED, 0);
        
        
    });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    beforeEach( async function () {
        cleared = await tableHandler.clear();
    });

    // /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        cleared = await tableHandler.clear();

        
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/