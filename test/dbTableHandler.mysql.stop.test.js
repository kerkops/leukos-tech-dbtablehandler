
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const schemas = require('./resources/tableSchemas');
const params = require('./resources/tableParams');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlTableHandler.stop(force=false)';

describe (`${getHeader(mainHeader, 0)}`, function() {

    let thisParams = jsutils.io.copyObj(params.goodParams);
    thisParams.connectionLimit = 321;

    /** Memorizza l'istante iniziale del test (SE TIMING ATTIVO). */ 
    let start;
    
    // Altre variabili con Scope locale a questa unità vanno qui
    
    

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`STP01 - Risolve in \`false\` e mantiene aperta la pool se il metodo stop viene invocato con altre istanze in uso`, async function(){

        let thisHeader = 'STP01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        // if ('<someVariable>') {
        //     /** Motivo dello skip */
        //     let reason = '';
        //     console.warn(`\t# TEST SKIPPED: ${reason} #`);
        //     this.skip();
        // } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [thisParams, schemas.userSch, DEBUG_ENABLED];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tested(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+"Creazione Singleton", hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res2 = await tested(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+"Creazione Singleton", hrtime(start), arguments)
        
        expect(res2.handlerName).equal(res.handlerName);
        expect(res2._copies).equal(1);
        let stop2Res = await res2.stop();
        expect(stop2Res).equal(false);
        let findRes = await res.find();
        expect(res._copies).equal(0);
        let stop1Res = await res.stop();
        expect(stop1Res).equal(true);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //



// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
//                   (rimuovere `async` per testase FUNZIONI SINCRONE)                          //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    // });


}); /** ---------------- fine della suite #1 ----------------------------------------*/