
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const schemas = require('./resources/tableSchemas');
const entries = require('./resources/dbEntries');
const params = require('./resources/tableParams');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlTableHandler.update(ids, keys, values, idKey="id")';

describe (`${getHeader(mainHeader, 0)}`, function() {
    this.timeout(0)
    
    // Variabili con Scope locale a questa unità vanno qui
    let tableHandler = null;
    let inserted = false;
    let insertedIds = null;
    let recordInUse = null;
    
    let timeRes;
   
    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`UPD01 - Risolve in \'queryResult.success: true\' e aggiorna i campi dei record con id valido e parametri corretti  `, async function(){

        let thisHeader = 'UPD01'
        let header = `${mainHeader} - ${thisHeader}`;

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [insertedIds, [keysInUse[2]], ['newValue'] ];
        
        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.update(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    it(`UPD02 - Risolve in \'queryResult.success: false\' e non aggiorna i campi dei record con id non valido `, async function(){

        let thisHeader = 'UPD02'
        let header = `${mainHeader} - ${thisHeader}`;

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [[777], [keysInUse[2]], ['newValue'] ];
        
        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.update(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(false);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    it(`UPD03 - Risolve in \'queryResult.success: false\' e non aggiorna i campi dei record con id valido e parametri non corretti  `, async function(){

        let thisHeader = 'UPD03'
        let header = `${mainHeader} - ${thisHeader}`;

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [insertedIds, ['veryWrongKey'], ['newValue'] ];
        
        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.update(...arguments);
        
        if (TIMEIT_ENABLED) {
            try{timeRes.add(thisHeader, hrtime(start), arguments)
            }catch(err){
                console.error(`Errore: ${err.name}-${err.message}`)
            }}
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(false);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/**
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  *                     IMPOSTAZIONI DEI TEST
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  */
    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) timeRes = new TimingResults(mainHeader)
        // Modificando il valore crea una istanza scollegata del driver (non viene chiusa dalle altre test suite)
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 212;
        tableHandler = await tested(par, schemas.userSch, DEBUG_ENABLED, 0);
        keysInUse = tableHandler._insertKeys;
    });

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
            if (TIMEIT_ENABLED) {
            console.info(timeRes.getResults())
            }
            await tableHandler.stop();
        
        
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    beforeEach( async function () {
        if (tableHandler){
            recordInUse = jsutils.io.copyObj( entries.users1.records[0] );

            let res = await tableHandler.insert(recordInUse);
            inserted = res.success;
            insertedIds = res.insertedIds;
        } else {
            inserted = false;
            insertedIds = null;
        }
    });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        cleared = await tableHandler.clear();
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/