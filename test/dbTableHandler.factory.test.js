
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED = (['1', 'true'].includes(env_DEBUG_PROP))
// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const expect = require('chai').expect;

const jsutils = require('leukos-tech-jsutils');
const getHeader = jsutils.tests.getHeader;

/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
/** Handler risultati timing */
const TimingResults = jsutils.tests.timeIt;

const hrtime = process.hrtime;



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const schemas = require('./resources/tableSchemas');

// const aVar = require('');
// const aVar = require('');
// const aVar = require('');

const params1 = {
    table: 'testTable',
    engine: 'mysql',
    connectionLimit: 10,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};

const goodParams2 = {
    table: 'testTable',
    engine: 'mysql',
    connectionLimit: 100,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};

const mainHeader = "Class Factory #factory #mysql"
describe (`${getHeader(mainHeader, 0)}`, function() {

    // Variabili con Scope locale a questa unità vanno qui
    let res;
    let start;
    let thisPar;
    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`FAC01 - Resituisce l'oggetto con parametri corretti`, async function(){
        let thisHeader = 'FAC01'
        let header = `${mainHeader} - ${thisHeader}`;

        // res = await tested(params1, schemas.userSch, DEBUG_ENABLED, 0);
        // if (DEBUG_ENABLED) console.debug(`${header} - Res: ${res}`);
        expect(res).not.equal(undefined);
        
        expect(typeof res.insert).equal('function');
        // await res._driver.stop();

        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`FAC02 - Restituisce sempre lo stesso oggetto (Singleton) in seguito a chiamate multiple coi medesimi valori`, async function(){
        let thisHeader = 'FAC02'
        let header = `${mainHeader} - ${thisHeader}`;

        // res = await tested(params1, schemas.userSch, DEBUG_ENABLED, 0);

        if (!res) this.skip(`La factory non ha restituito l'oggetto primario. `)

        let cfr;
        
        for (let i=1; i<=10; i++) {

            if (TIMEIT_ENABLED) start = hrtime();
            cfr = await tested(thisPar, schemas.userSch, DEBUG_ENABLED, 1);
            if (TIMEIT_ENABLED) timeRes.add(`${thisHeader} n.${i}`, hrtime(start), [])
            
            expect(cfr.handlerName).equal(res.handlerName)
            if (DEBUG_ENABLED) console.info(`Test n. ${i} - Superato: Stesso oggetto`)
            
        }
        // await res._driver.stop();
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`FAC03 - Restituisce istanze tra loro interconnesse`, async function(){
        let thisHeader = 'FAC03';
        let header = `${mainHeader} - ${thisHeader}`;

        // res = await tested(params1, schemas.userSch, DEBUG_ENABLED, 0);

        if (!res) this.skip(`La factory non ha restituito l'oggetto primario. `);

        if (TIMEIT_ENABLED) start = hrtime();
        let cfr = await tested(thisPar, schemas.userSch, DEBUG_ENABLED, 0);
        if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), [])

        if (!cfr) this.skip(`La factory non ha restituito l'oggetto secondario. `);

        cfr.newProp = 1;

        expect(res.newProp).equal(1);

        // await res._driver.stop();


    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`FAC04 - Restituisce istanze diverse con parametri diversi, tra loro non connesse`, async function(){
        let thisHeader = 'FAC04'
        let header = `${mainHeader} - ${thisHeader}`;

        // res = await tested(params1, schemas.userSch, DEBUG_ENABLED, 0);

        if (!res) this.skip(`La factory non ha restituito l'oggetto primario. `);
        if (TIMEIT_ENABLED) start = hrtime();
        let cfr = await tested(goodParams2, schemas.userSch, DEBUG_ENABLED, 0);
        if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), [])
        if (!cfr) this.skip(`La factory non ha restituito l'oggetto secondario. `);

        expect(res.handlerName).not.equal(cfr.handlerName);
        res.newProp = 5;
        

        expect(cfr.newProp).equal(undefined);

        // await res._driver.stop();
        await cfr._driver.stop();

    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`FAC05 - Restituisce istanze diverse che condividono lo stesso Singleton per il driver con parametri uguali in tutto tranne che per il nome della table`, async function(){
        let thisHeader = 'FAC05'
        let header = `${mainHeader} - ${thisHeader}`;
        
        let thisPar2 = jsutils.io.copyObj(params1);
        thisPar2.connectionLimit = 201;
        thisPar2.table = "testTable2"
        
        // res = await tested(params1, schemas.userSch, DEBUG_ENABLED, 0);

        if (!res) this.skip(`La factory non ha restituito l'oggetto primario. `);
        if (TIMEIT_ENABLED) start = hrtime();
        let cfr = await tested(thisPar2, schemas.userSch, DEBUG_ENABLED, 0);
        if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), [])
        if (!cfr) this.skip(`La factory non ha restituito l'oggetto secondario. `);

        expect(res.handlerName).not.equal(cfr.handlerName);
        res.newProp = 5;
        cfr.newProp = 1;

        expect(res.newProp).equal(5);

        let driverName1 = res._driver._pool.poolName;
        let driverName2 = cfr._driver._pool.poolName;

        expect(driverName1).equal(driverName2);

        // await res._driver.stop();
        await cfr._driver.stop();

    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    
 /**
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  *                     IMPOSTAZIONI DEI TEST
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  */
    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        
        let par = jsutils.io.copyObj(params1);
        par.connectionLimit = 201;
        thisPar = par;
        let parameters = [thisPar, schemas.userSch, DEBUG_ENABLED, 0]
        if (TIMEIT_ENABLED) start = hrtime();
        res = await tested(...parameters);
        if (TIMEIT_ENABLED) timeRes.add(`(Chiamata che genera il Singleton)`, hrtime(start), [])
    });

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
        await res._driver.stop();
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - after() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/