const users1 = {
    keys : [ 'username', 'email', 'password', 'userpic', 'created' ],
    records: [
        ['username_luca', 'email_luca', 'password_luca', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_sofia', 'email_sofia', 'password_sofia', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_mario', 'email_mario', 'password_mario', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_luigi', 'email_luigi', 'password_luigi', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_lilith', 'email_lilith', 'password_lilith', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_marta', 'email_marta', 'password_marta', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_zangief', 'email_zangief', 'password_zangief', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_chun-li', 'email_chun-li', 'password_chun-li', null, new Date().toISOString().slice(0, 19).replace('T', ' ')]
        
    ] 
};

const companies1 = {
    keys : ['name'],
    records : [['civico4'], ['blind pig'], ['mc donalds'], ['macrosolution']]
};


module.exports = {
    users1: users1,
    companies1: companies1
}