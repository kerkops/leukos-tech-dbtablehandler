const userTable ='users_test_table';
const companiesTable ='companies_test_table';
const userCompanyTable ='users_companies_test_table';

const userSchema = [
    
    {   
    // [Mandatory]
    keyName:        "id",
    type:           "int", 
    

    // .type può essere: 
    // * 'string', 'char'
    // * 'number', 'int', 'integer', 'intero'
    // * 'float', 'double'
    // * 'bool', 'boolean'
    // * 'date', 'datetime', 'time', 'timestamp'
    // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
    // [Opzionali]
    size:           undefined, 
    isUnsigned:     true,
    autoIncrement:  true,
    notNull:        false,
    isPrimary:      true,
    isIndex:        false,
    isUnique:       true,
    isReference:    false,
    reference: 
        {
        tableName:  undefined,
        tableField: undefined
        //onUpdate: undefined,
        //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "username", // Nome del campo
        type:           "string", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      false,
        isIndex:        true,
        isUnique:       true,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "email", // Nome del campo
        type:           "string", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      false,
        isIndex:        true,
        isUnique:       true,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "password", // Nome del campo
        type:           "string", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           500, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      false,
        isIndex:        false,
        isUnique:       false,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "userpic", // Nome del campo
        type:           "image", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        false,
        isPrimary:      false,
        isIndex:        false,
        isUnique:       false,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "created", // Nome del campo
        type:           "datetime", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      false,
        isIndex:        false,
        isUnique:       false,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    }
    
];

const companySchema = [
    {   
        // [Mandatory]
        keyName:        "id", // Nome del campo
        type:           "int", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     true,
        autoIncrement:  true,
        notNull:        false,
        isPrimary:      true,
        isIndex:        false,
        isUnique:       true,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },

    {   
        // [Mandatory]
        keyName:        "name", // Nome del campo
        type:           "string", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           50, 
        isUnsigned:     false,
        autoIncrement:  false,
        notNull:        false,
        isPrimary:      false,
        isIndex:        true,
        isUnique:       true,
        isReference:    false,
        reference: 
            {
            tableName:  undefined,
            tableField: undefined
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    }
    
    
]; // fine companySchema

const userCompanySchema = [
    {   
        // [Mandatory]
        keyName:        "company_id", // Nome del campo
        type:           "int", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     true,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      true,
        isIndex:        false,
        isUnique:       false,
        isReference:    true,
        reference: 
            {
            tableName:  companiesTable,
            tableField: 'id'
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    },
    
    {   
        // [Mandatory]
        keyName:        "user_id", // Nome del campo
        type:           "int", // Valore contenuto (data type)
    
    
        // .type può essere: 
        // * 'string', 'char'
        // * 'number', 'int', 'integer', 'intero'
        // * 'float', 'double'
        // * 'bool', 'boolean'
        // * 'date', 'datetime', 'time', 'timestamp'
        // * 'blob', 'image', 'audio', 'mediumblob', 'longblob' 
    
        // [Opzionali]
        size:           undefined, 
        isUnsigned:     true,
        autoIncrement:  false,
        notNull:        true,
        isPrimary:      true,
        isIndex:        false,
        isUnique:       false,
        isReference:    true,
        reference: 
            {
            tableName:  userTable,
            tableField: 'id'
            //onUpdate: undefined,
            //onDelete: undefined, (CASCADE, DELETE, RESTRICT)
        }
    }
    
]; // fine userCompanySchema

module.exports= {
    userTable: userTable,
    userSch: userSchema,
    companiesTable: companiesTable,
    companiesSch: companySchema,
    userCompaniesTable: userCompanyTable,
    userCompanies: userCompanySchema
};
