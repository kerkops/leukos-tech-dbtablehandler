
// Stringa utilizzata per require()
const TESTED_MODULE = "../lib/"; // la path del modulo da testare va qui

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../lib/mysql_tablehandler")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED = (['1', 'true'].includes(env_DEBUG_PROP)) ? true : false

// console.warn(`DEBUG ENABLED? ${DEBUG_ENABLED} (env Prop = ${env_DEBUG_PROP})`);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const expect = require('chai').expect;

const jsutils = require('leukos-tech-jsutils');
const getHeader = jsutils.tests.getHeader;


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
// const aVar = require('');
// const aVar = require('');
// const aVar = require('');


describe (`${getHeader('Costruttore della Classe MysqlTableHandler(debug, logger, logLevel)', 0)}`, function() {

    // Variabili con Scope locale a questa unità vanno qui
    let mainHeader = `MysqlTableHandler(debug, logger, logLevel)`;
    this.timeout(0)
    

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()  - (`async` se la funzione da testare è asyncrona (cb o promise) ~~~~~~~~~~~~~~~~~~ //`
    it(`TC001 - Genera le proprietà necessarie al debug in accordo agli argomenti`, function(){
        let thisHeader = 'TC001'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        // if ('<someVariable>') {
        //     console.warn(`\t# TEST SKIPPED: <Motivo dello skip> #`);
        //     this.skip();
        // } 
        let debug = DEBUG_ENABLED;
        let logLevels = 0
        let res = new tested(debug, console, logLevels);

        if (DEBUG_ENABLED) console.debug(`${header} - Proprietà generate per l'oggetto: ${Object.keys(res)}`);
        if (DEBUG_ENABLED) console.debug(`${header} - Valore di _debugEnabled: ${res._debugEnabled}; Valore di _logLevel: ${res._logLevel}`);
        expect(res._debugEnabled).equal(debug);
        expect(res._logLevel).equal(logLevels);

        expect(typeof res._log).equal("function");
        

    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //



 /**
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  *                     IMPOSTAZIONI DEI TEST
  * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
  */
    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    // before( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    // after( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - after() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - after() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - BeforeEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    // /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    //     try {
    //         // Codice da eseguire prima di TUTTA LA SUITE QUI 
    //     } catch (err) {
    //         err.message = `${mainHeader} - afterEach() fallita - ${err.name} - ${err.message}`;
    //         if (DEBUG_ENABLED) console.warn(err.message);
    //     }
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/