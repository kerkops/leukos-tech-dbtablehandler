
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const schemas = require('./resources/tableSchemas');
const entries = require('./resources/dbEntries');
const params = require('./resources/tableParams');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlTableHandler.findById(id, idKey="id")';

describe (`${getHeader(mainHeader, 0)}`, function() {

    // Variabili con Scope locale a questa unità vanno qui
    
    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    let tableHandler = null;
    let inserted = false;
    cleared = false;
    let insertedIds = null;
    let recordInUse = null;
    let keysInUse = null;
    let timeRes;
    
    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`FID01 - Risolve in \`queryResult.success: true\` e restituisce il record se fornito un id valido`, async function(){

        let thisHeader = 'FID01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [insertedIds[0]];
        
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.findById(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        // console.info(res.data)
        for (let key of keysInUse) {
            // console.info(`${JSON.stringify(res.data[0][key])} - ${recordInUse}`)
            if (key!='created') expect(recordInUse.includes(res.data[0][key])).equal(true)
        }
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`FID02 - Risolve in \`queryResult.success: true\` e restituisce tutti i record se non fornito nessun id`, async function(){
        this.timeout(0)
    
        let thisHeader = 'FID02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = null;
        
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.findById(null);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        // console.info(res)
        
        expect(res.success).equal(true);
        // console.info(res.data)
        for (let key of keysInUse) {
            // console.info(`${JSON.stringify(res.data[0][key])} - ${recordInUse}`)
            if (key!='created') expect(recordInUse.includes(res.data[0][key])).equal(true)
        }
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`FID03 - Risolve in \`queryResult.success: false\` e non restituisce alcun record se fornito un id non valido`, async function(){

        let thisHeader = 'FID03'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!inserted) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento dati a scopo preparatorio';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = 111;
        
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await tableHandler.findById(1111);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments.toString())
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        // console.info(res)
        // console.info(res.data)
        expect(res.success).equal(false);
        expect(res.data.length).equal(0)
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    

// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 203;
        tableHandler = await tested(par, schemas.userSch, DEBUG_ENABLED, 0);
        keysInUse = tableHandler._insertKeys;
    });

    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        await tableHandler.stop();
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    beforeEach( async function () {
        if (tableHandler){
            recordInUse = jsutils.io.copyObj( entries.users1.records[0] );

            let res = await tableHandler.insert(recordInUse);
            inserted = res.success;
            insertedIds = res.insertedIds;
        } else {
            inserted = false;
            insertedIds = null;
        }
    });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        cleared = await tableHandler.clear();
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/